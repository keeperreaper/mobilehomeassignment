package com.example.gamingdatabaseapplication;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import static android.content.Context.MODE_PRIVATE;
import static com.facebook.FacebookSdk.getApplicationContext;

public class FavoriteFragment extends Fragment {

    public static Fragment newInstance() {
        DetailGameFragment searchFragment = new DetailGameFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        if(readFile() != null){
            args.putString("game", readFile());
            searchFragment.setArguments(args);
        }
        return searchFragment;
    }

    List<List<String>> data = new ArrayList<>();

    public static String readFile() {
        try {
            FileInputStream fileInputStream = getApplicationContext().openFileInput("Favorites.txt");
            InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

            BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
            StringBuffer stringBuffer = new StringBuffer();

            String lines;
            while ((lines = bufferedReader.readLine()) != null) {
                return lines;
            }

            //displayText.setText(stringBuffer.toString());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState){
        try {

            Bundle args = getArguments();
            String searchField;
            if(args == null)
                searchField = "nothing";
            else
                searchField = args.getString("game"," ");

            data = new JSONRequester().execute("https://api-v3.igdb.com/games/" + searchField + "?fields=name, release_dates.y, cover.url, summary").get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        View view = inflater.inflate(R.layout.detailed_view_fragment,container,false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new RecyclerViewAdapter(data));

        return view;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private CardView mCardView;
        private TextView mTitleView,
                mYearView,mDescriptionView;
        private ImageView mImageView;
        private Button mFavoriteBtn;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
        }


        public  RecyclerViewHolder(LayoutInflater inflater, ViewGroup container){

            super(inflater.inflate(R.layout.detailed_card_view,container,false));

            mCardView = itemView.findViewById(R.id.detail_cardview);
            mTitleView = itemView.findViewById(R.id.title_holder);
            mYearView = itemView.findViewById(R.id.year_holder);
            mImageView = itemView.findViewById(R.id.image_Holder);
            mDescriptionView = itemView.findViewById(R.id.description_holder);
            mFavoriteBtn = itemView.findViewById(R.id.favorite_btn);

        }
    }

    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder>{
        private List<String> mNameList = new ArrayList<>();
        private List<String> mYearList = new ArrayList<>();
        private List<String> mImageList = new ArrayList<>();
        private List<String> mIDList = new ArrayList<>();
        private List<String> mDescriptionList = new ArrayList<>();

        public RecyclerViewAdapter(List<List<String>> data){
            for(List<String> row : data){
                Log.v("Here", "Double array to array: " + row.get(0));
                mNameList.add(row.get(0));

                if(row.size() > 1)
                    mImageList.add(row.get(1));

                mYearList.add(row.get(2));
                mIDList.add(row.get(3));

                mDescriptionList.add(row.get(4));
            }
        }

        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder( ViewGroup parent, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());

            return new RecyclerViewHolder(inflater, parent);
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolder holder, final int pos) {
            holder.mTitleView.setText(mNameList.get(pos));
            holder.mYearView.setText(mYearList.get(pos));
            holder.mDescriptionView.setText(mDescriptionList.get(pos));

            holder.mFavoriteBtn.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            writeFile(mIDList.get(pos));
                            Log.wtf("Here","Pressed holder no " + mIDList.get(pos));
                        }
                    }
            );

            if(mImageList.get(pos) != null){
                Log.v("Here", "Image debug recycler: " + mImageList.get(pos));

                Picasso.get().load(mImageList.get(pos)).into(holder.mImageView);

            }
        }
        public boolean readFile(String gameId) {
            try {
                FileInputStream fileInputStream = getApplicationContext().openFileInput("Favorites.txt");
                InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);

                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                StringBuffer stringBuffer = new StringBuffer();

                String lines;
                while ((lines = bufferedReader.readLine()) != null) {
                    if(lines.contains(gameId))
                        return true;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return false;
        }
        public void writeFile(String gameId) {

            try {
                if(!readFile(gameId)){
                    FileOutputStream fileOutputStream = getApplicationContext().openFileOutput("Favorites.txt", MODE_PRIVATE);
                    fileOutputStream.write(gameId.getBytes());
                    fileOutputStream.close();

                    Toast.makeText(getApplicationContext(), "Favorited!", Toast.LENGTH_SHORT).show();

                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            if(mNameList == null)
                return 0;
            return mNameList.size();
        }
    }

}
