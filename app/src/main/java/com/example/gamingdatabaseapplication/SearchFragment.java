package com.example.gamingdatabaseapplication;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;


public class SearchFragment extends Fragment {

    public static Fragment newInstance(String gameSearch,String searchingField) {
        SearchFragment searchFragment = new SearchFragment();
        // Supply index input as an argument.
        Bundle args = new Bundle();
        args.putString("gameSearch", gameSearch);
        args.putString("searchingField", searchingField);
        searchFragment.setArguments(args);
        return searchFragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState){

        try {
            List<List<String>> data;

            Bundle args = getArguments();
            String searchField, searchingField;
            if(args == null) {
                searchField = "";
                searchingField = "";
            }else{
                Log.wtf("Here", "In the searchfield " + args.getString("search",""));
                searchField = "&search=" + args.getString("gameSearch","");
                searchingField = args.getString("searchingField","");
            }


            data = new JSONRequester().execute("https://api-v3.igdb.com/games/?fields=name, release_dates.y, cover.url" + searchField).get();

            View view = inflater.inflate(R.layout.search_view_fragment,container,false);
            RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(new RecyclerViewAdapter(data));
            EditText editText = view.findViewById(R.id.search_field);
            editText.setText(searchingField, TextView.BufferType.EDITABLE);
            return view;


        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return null;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private CardView mCardView;
        private TextView mTitleView,
                mYearView;
        private ImageView mImageView;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
        }

        public  RecyclerViewHolder(LayoutInflater inflater, ViewGroup container){
            super(inflater.inflate(R.layout.card_view,container,false));

            mCardView = itemView.findViewById(R.id.search_cardview);
            mTitleView = itemView.findViewById(R.id.title_holder);
            mYearView = itemView.findViewById(R.id.year_holder);
            mImageView = itemView.findViewById(R.id.image_Holder);
        }
    }

    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder>{
        private List<String> mNameList = new ArrayList<>();
        private List<String> mYearList = new ArrayList<>();
        private List<String> mImageList = new ArrayList<>();
        private List<String> mIDList = new ArrayList<>();

        public RecyclerViewAdapter(List<List<String>> data){
            for(List<String> row : data){
                Log.v("Here", "Double array to array: " + row.get(0));
                mNameList.add(row.get(0));

                if(row.size() > 1)
                    mImageList.add(row.get(1));

                mYearList.add(row.get(2));
                mIDList.add(row.get(3));

            }
        }

        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder( ViewGroup parent, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());

            return new RecyclerViewHolder(inflater, parent);
        }

        @Override
        public void onBindViewHolder(RecyclerViewHolder holder, final int pos) {
            holder.mCardView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            getFragmentManager().beginTransaction().replace(R.id.fragment_container,
                                    new DetailGameFragment().newInstance(mIDList.get(pos))
                            ).commit();
                            Log.wtf("Here","Pressed holder no " + mIDList.get(pos));
                        }
                    }
            );

            holder.mTitleView.setText(mNameList.get(pos));
            holder.mYearView.setText(mYearList.get(pos));
            if(mImageList.get(pos) != null){
                Log.v("Here", "Image debug recycler: " + mImageList.get(pos));

                Picasso.get().load(mImageList.get(pos)).into(holder.mImageView);

            }
        }


        @Override
        public int getItemCount() {
            if(mNameList == null)
                return 0;
            return mNameList.size();
        }
    }

}
