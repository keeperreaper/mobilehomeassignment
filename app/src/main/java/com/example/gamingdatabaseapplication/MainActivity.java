package com.example.gamingdatabaseapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.google.firebase.auth.FirebaseAuth;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MainActivity  extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FirebaseAuth mAuth;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    private EditText editText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        navigationView.setNavigationItemSelectedListener(this);
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if(!(fragment instanceof HomeFragment)){
                fragment = new HomeFragment();
                getSupportFragmentManager().
                        beginTransaction().replace(R.id.fragment_container,
                        fragment).commit();

            }
            else
                super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            fragment = new HomeFragment();
            FirebaseAuth.getInstance().signOut();
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public void onSearch(View v)
    {

        EditText editText = findViewById(R.id.search_field);
        SearchFragment searchFragment = new SearchFragment();

        mEditor = mPreferences.edit();
        mEditor.putString("gameSearch", editText.getText().toString());
        mEditor.commit();

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                searchFragment.newInstance(mPreferences.getString("gameSearch",""), editText.getText().toString())

        ).commit();
        //Log.wtf("Here","pressed edit text");
    }

    Fragment previousFragment = null;
    Fragment fragment = null;

    @Override
    public void onStart(){
        super.onStart();

        if(mPreferences != null && !mPreferences.getString("search","").equals("")){
            fragment = new SearchFragment();
            SearchFragment searchFragment = new SearchFragment();

            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
            Snackbar.make(drawerLayout, "Last time Stop: " + mPreferences.getString("timedate",""), Snackbar.LENGTH_LONG).show();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    searchFragment.newInstance(mPreferences.getString("gameSearch",""), mPreferences.getString("searchingField",""))

            ).commit();
        }else{
            fragment = new HomeFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
        }
    }

    @Override
    public void onResume(){
        super.onResume();

        if(mPreferences != null && !mPreferences.getString("search","").equals("")){
            fragment = new SearchFragment();
            SearchFragment searchFragment = new SearchFragment();

            DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);



            Snackbar.make(drawerLayout, "Last time Stop: " + mPreferences.getString("timedate",""), Snackbar.LENGTH_LONG).show();



            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container,
                    searchFragment.newInstance(mPreferences.getString("gameSearch",""), mPreferences.getString("searchingField",""))

            ).commit();
        }else{
            fragment = new HomeFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
        }
    }

    void saveDate(){
        mEditor = mPreferences.edit();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        mEditor.putString("timedate", dateFormat.format(date));
        mEditor.commit();
    }

    @Override
    public void onPause() {
        saveDate();
        if(fragment instanceof SearchFragment){
            mEditor = mPreferences.edit();
            mEditor.putString("searchingField", ((EditText)findViewById(R.id.search_field)).getText().toString());
            mEditor.commit();
        }
        super.onPause();
    }

    @Override
    public void onStop() {
        saveDate();
        if(fragment instanceof SearchFragment){
            mEditor = mPreferences.edit();
            mEditor.putString("searchingField", ((EditText)findViewById(R.id.search_field)).getText().toString());
            mEditor.commit();
        }
        super.onStop();
    }

    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        previousFragment = fragment;
        if (id == R.id.nav_home) {
            fragment = new HomeFragment();
        }else if (id == R.id.nav_search) {
            fragment = new SearchFragment();
        }else if (id == R.id.nav_favorites) {
            fragment = FavoriteFragment.newInstance();
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, fragment).commit();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
