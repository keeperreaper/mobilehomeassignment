package com.example.gamingdatabaseapplication;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class HomeFragment extends Fragment {

    public static Fragment newInstance() {
        return new SearchFragment();
    }


    List<List<String>> popularData = new ArrayList<>(),
            comingSoon = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState){

        try {

            popularData = new JSONRequester().execute("https://api-v3.igdb.com/games/?fields=name, release_dates.y, cover.url&order=popularity:desc").get();

            comingSoon = new JSONRequester().execute("https://api-v3.igdb.com/release_dates/?fields=game&order=date:desc&filter[platform][eq]=48&filter[date][gt]="+System.currentTimeMillis()+"&expand=games").get();
            String comingSoonGames = "";
            for(List<String> row : comingSoon){
                comingSoonGames += row.get(0) + ", ";
            }
            comingSoonGames = comingSoonGames.substring(0, comingSoonGames.length() - 2);

            comingSoon = new JSONRequester().execute("https://api-v3.igdb.com/games/"+comingSoonGames+"?fields=name, release_dates.y, cover.url&order=popularity:desc").get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        View view = inflater.inflate(R.layout.home_view_fragment,container,false);
        RecyclerView recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        recyclerView.setAdapter(new RecyclerViewAdapter(popularData));


        RecyclerView recyclerView2 = view.findViewById(R.id.recycler_view2);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false));
        recyclerView2.setAdapter(new RecyclerViewAdapter(comingSoon));
        return view;
    }

    private class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private CardView mCardView;
        private TextView mTitleView,
                mYearView;
        private ImageView mImageView;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
        }

        public  RecyclerViewHolder(LayoutInflater inflater, ViewGroup container){
            super(inflater.inflate(R.layout.home_card_view,container,false));

            mCardView = itemView.findViewById(R.id.home_cardview);
            mTitleView = itemView.findViewById(R.id.title_holder);
            mYearView = itemView.findViewById(R.id.year_holder);
            mImageView = itemView.findViewById(R.id.image_Holder);
        }
    }

    private class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewHolder>{
        private List<String> mNameList = new ArrayList<>();
        private List<String> mYearList = new ArrayList<>();
        private List<String> mImageList = new ArrayList<>();

        public RecyclerViewAdapter(List<List<String>> data){
            for(List<String> row : data){
                Log.v("Here", "Double array to array: " + row.get(0));
                mNameList.add(row.get(0));

                if(row.size() > 1)
                    mImageList.add(row.get(1));

                mYearList.add(row.get(2));
            }
        }

        @NonNull
        @Override
        public RecyclerViewHolder onCreateViewHolder( ViewGroup parent, int i) {
            LayoutInflater inflater = LayoutInflater.from(getActivity());

            return new RecyclerViewHolder(inflater, parent);
        }

        @Override
        public void onBindViewHolder( RecyclerViewHolder holder, int pos) {
            holder.mTitleView.setText(mNameList.get(pos));
            holder.mYearView.setText(mYearList.get(pos));
            if(mImageList.get(pos) != null){
                Log.v("Here", "Image debug recycler: " + mImageList.get(pos));

                Picasso.get().load(mImageList.get(pos)).into(holder.mImageView);

            }
        }
        @Override
        public int getItemCount() {
            if(mNameList == null)
                return 0;
            return mNameList.size();
        }
    }

}
