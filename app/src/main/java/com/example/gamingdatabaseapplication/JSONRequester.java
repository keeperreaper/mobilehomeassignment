package com.example.gamingdatabaseapplication;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class JSONRequester extends AsyncTask<String, Void, List<List<String>>> {


    @Override
    protected List<List<String>> doInBackground(String... url) {
        List<List<String>> data = new ArrayList<List<String>>();

        HttpURLConnection con = null;
        InputStream is = null;

        try {
            con = (HttpURLConnection) new URL(url[0]).openConnection();
            con.setRequestMethod("GET");
            con.addRequestProperty("user-key", "4aca9ca2e8f39e942671ae896917b4a4");
            con.setReadTimeout(15000);
            con.setConnectTimeout(15000);
            con.setDoOutput(false);
            con.connect();


            // Let's read the response
            StringBuffer buffer = new StringBuffer();
            is = con.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line = null;
            StringBuilder sb = new StringBuilder();

            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            JSONObject jObj = null;
            String jsonArrayString = sb.toString();
            JSONArray jsonArray = new JSONArray(jsonArrayString);
            //jObj = new JSONObject(jsonArray);
            for (int i = 0; i < jsonArray.length(); i++) {
                data.add(new ArrayList<String>());
                JSONObject jsonobject = jsonArray.getJSONObject(i);


                if (url[0].contains("https://api-v3.igdb.com/release_dates/?fields=game&order=date:desc&filter[platform][eq]=48&filter[date][gt]")) {
                    data.get(data.size() - 1).add(jsonobject.getString("game"));
                } else {

                    data.get(data.size() - 1).add(jsonobject.getString("name"));

                    if (jsonobject.has("cover")) {
                        data.get(data.size() - 1).add("https:" + jsonobject.getJSONObject("cover").getString("url").replace("t_thumb", "t_cover_big"));
                    } else {
                        data.get(data.size() - 1).add(null);
                    }

                    if (jsonobject.has("release_dates")) {
                        JSONObject releaseDate = jsonobject.getJSONArray("release_dates").getJSONObject(0);
                        if (releaseDate.has("y"))
                            data.get(data.size() - 1).add(2, releaseDate.getString("y"));
                        else
                            data.get(data.size() - 1).add(2, "TBD");
                    } else {
                        data.get(data.size() - 1).add(2, "TBD");
                    }

                    data.get(data.size() - 1).add(3, jsonobject.getString("id"));

                    if (jsonobject.has("summary")) {
                        data.get(data.size() - 1).add(4, jsonobject.getString("summary"));
                    } else {
                        data.get(data.size() - 1).add(4, "No summary given");
                    }
                }
            }
            is.close();
            con.disconnect();

            return data;
        }
        catch(Exception e) {
            Log.wtf("Here", "error in sql connection: " + e);
            e.printStackTrace();
        }
        finally {
            try { is.close(); } catch(Throwable t) {}
            try { con.disconnect(); } catch(Throwable t) {}
        }
        return null;
    }

}