package com.example.gamingdatabaseapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.goodiebag.pinview.Pinview;

public class PinActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pin_activity);

        Pinview pv = findViewById(R.id.pinView);
        pv.setPinViewEventListener(new Pinview.PinViewEventListener() {
            @Override
            public void onDataEntered(Pinview pv, boolean b) {
                if (Integer.parseInt(pv.getValue()) == 1234)
                {
                    Intent nextIntent = new Intent(PinActivity.this, LoginActivity.class);
                    startActivity(nextIntent);
                }
            }
        });
    }
}
